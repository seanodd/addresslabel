﻿using System;
class Program
{
    static void Main(string[] args)
    {
        Console.WriteLine("Hello world!");
        string name = "Sean";
        string streetAddress = "1234 Woodward Ave";
        int aptNum = 45;
        string city = "Detroit";
        string state = "Michigan";
        int zip = 48201;


        Console.WriteLine($"{name}");
        Console.WriteLine($"{streetAddress} {aptNum}");
        Console.WriteLine($"{city}, {state} {zip}");
       
    }
}
